 -----pulso_largo   : buffer std_logic --se cactiva cuando una tecla está pulsada más de 2s. Tmuestreo_tecla = 5ms --> 2s/5ms = 400 ciclos
 -----pulso corto: detecta una tecla con duración < 







library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

entity ctrl_tec is

  port (
    clk           : in std_logic;
    nRst          : in std_logic;
    tic           : in std_logic;
    columna       : in std_logic_vector(3 downto 0);
    fila          : buffer std_logic_vector(3 downto 0);
    tecla_pulsada : buffer std_logic; ---pulso corto
    tecla         : buffer std_logic_vector(3 downto 0);
    pulso_largo   : buffer std_logic --se cactiva cuando 
  );
end entity;

architecture rtl of ctrl_tec is
  signal tecla_hex    : std_logic_vector(3 downto 0);

  signal tecla_activa : std_logic; --variable que muestra se si ha pulado una tecla

  signal filtrado_Columna_1: std_logic_vector(3 downto 0);
  signal columna_sin_rebotes: std_logic_vector(3 downto 0);

  
  signal tecla_aux: std_logic;
  signal cuenta_2s: std_logic_vector(8 downto 0);
  --constant tics_2s : natural:= 400;--Numeros de tic(5ms) para 2 segundos
  constant tics_2s : natural:= 10;--Numeros de tic(5ms) para 2 segundos (escalado)
  signal ena_cuenta: std_logic;
  signal pulso_largo_aux: std_logic;
  signal ena_m: std_logic;


  




begin

-----FILTRO ANTI-REBOTES------
  FILTRO_1: process (clk, nRst)
   begin
      if nRst = '0' then
         filtrado_columna_1 <= (others => '1');
      elsif clk'event and clk = '1' then
         filtrado_columna_1 <= columna;
      end if;
  end process;

  FILTRO_2: process (clk, nRst)
   begin
      if nRst = '0' then
         columna_sin_rebotes <= (others => '1');
      elsif clk'event and clk = '1' then
        if tic ='1' then
         columna_sin_rebotes <= filtrado_columna_1;
        end if;
      end if;
  end process;
------FIN FILTRO ANTI-REBOTES----
tecla_activa <= columna_sin_rebotes(0) and columna_sin_rebotes(1) and columna_sin_rebotes(2) and columna_sin_rebotes(3);
ena_m <= filtrado_columna_1(0) and filtrado_columna_1(1) and filtrado_columna_1(2) and filtrado_columna_1(3);
--Tecla activa funciona a nivel bajo(-__-__)

------REGISTRO DE DESPLAZAMIENTO PARA FILAS-----
REG_DESPL: process (clk, nRst)
begin
   if nRst = '0' then
      fila<="1110";
   elsif clk'event and clk = '1' then
      if tic ='1' and ena_m = '1' then
        fila<= fila(2 downto 0) & fila(3);
        end if;
   end if;
end process;
------FIN REG DE DEPLAZAMIENTO---------------

------CONFORMADOR DE PULSOS-----
CONF_PULSOS: process (clk, nRst)
begin
   if nRst = '0' then
      tecla_aux<='1';
   elsif clk'event and clk = '1' then
    tecla_aux<=tecla_activa; 
    end if;
end process;

tecla_pulsada<='1' when tecla_aux='0' and tecla_activa='1' and cuenta_2s /= 10 else
               '0';
------FIN DE CONFORMADOR------------
        
------DETECTOR PULSO_LARGO---------
ena_cuenta<= '1' when tecla_activa ='0' and pulso_largo_aux = '0' else
             '0';

DETC_PULS_LARGO: process (clk, nRst)
begin
   if nRst = '0' then	
      cuenta_2s<=(others=>'0');
   elsif clk'event and clk = '1' then
      --if pulso_largo_aux = '0' and ena_cuenta = '1' and tecla_pulsada='1' then
     if pulso_largo = '0' then
      if pulso_largo_aux = '0' and ena_cuenta = '1' then
        cuenta_2s<=(0=>'1', others=>'0');
      elsif tecla_activa='1' then
        cuenta_2s<=(others=>'0');
      elsif pulso_largo_aux ='1' then
      if tic='1' then 
       if cuenta_2s /= tics_2s then
        cuenta_2s<= cuenta_2s + 1;
        else
        cuenta_2s<= (others=>'0');
        end if;
      end if;
      end if;
   end if;
end if;
end process;

pulso_largo_aux<='1' when cuenta_2s < tics_2s and cuenta_2s > 0 and tecla_activa='0' else 
                 '0';

pulso_largo<='1' when cuenta_2s=tics_2s and tecla_activa ='0' else
             '0';
------FIN DETECTOR PULSO_LARGO---------



------CODIFICADOR Y DECODIFICADOR------

         tecla_hex <=  X"0" when fila = "0111" and columna_sin_rebotes = "1101" else
         X"1" when fila = "1110" and columna_sin_rebotes = "1110" else 
         X"2" when fila = "1110" and columna_sin_rebotes = "1101" else 
         X"3" when fila = "1110" and columna_sin_rebotes = "1011" else 
         X"4" when fila = "1101" and columna_sin_rebotes = "1110" else 
         X"5" when fila = "1101" and columna_sin_rebotes = "1101" else 
         X"6" when fila = "1101" and columna_sin_rebotes = "1011" else 
         X"7" when fila = "1011" and columna_sin_rebotes = "1110" else 
         X"8" when fila = "1011" and columna_sin_rebotes = "1101" else 
         X"9" when fila = "1011" and columna_sin_rebotes = "1011" else 
         X"A" when fila = "0111" and columna_sin_rebotes = "1110" else 
         X"B" when fila = "0111" and columna_sin_rebotes = "1011" else 
         X"C" when fila = "0111" and columna_sin_rebotes = "0111" else 
         X"D" when fila = "1011" and columna_sin_rebotes = "0111" else 
         X"E" when fila = "1101" and columna_sin_rebotes = "0111" else 
         X"F" when fila = "1110" and columna_sin_rebotes = "0111" else 
         "XXXX";

         
        --Conversor de las teclas de Binario a Hexadecimal
        tecla <=  "0000" when tecla_hex = X"0" else
                  "0001" when tecla_hex = X"1" else
                  "0010" when tecla_hex = X"2" else
                  "0011" when tecla_hex = X"3" else
                  "0100" when tecla_hex = X"4" else
                  "0101" when tecla_hex = X"5" else
                  "0110" when tecla_hex = X"6" else
                  "0111" when tecla_hex = X"7" else
                  "1000" when tecla_hex = X"8" else
                  "1001" when tecla_hex = X"9" else
                  "1010" when tecla_hex = X"A" else
                  "1011" when tecla_hex = X"B" else
                  "1100" when tecla_hex = X"C" else
                  "1101" when tecla_hex = X"D" else
                  "1110" when tecla_hex = X"E" else
                  "1111" when tecla_hex = X"F" else
                  "XXXX";

------FIN CODIFICADOR Y DECODIFICADOR------
 
end rtl;
