---------------RESUMEN DE PRUEBAS MEDIANTE ESTIMULOS----------------------------
--1)Probar modos de funcionamiento (12h y 24h) y cambio de AM a PM
----1.1)Arrancamos con las caracteristicas por defecto (modo 12h y AM)
----1.2)Probamos a dar una vuelta entera en el modo 12h y cambiamos al modo 24h estando en AM
----1.3)Probamos a dar una vuelta entera en el modo 24h y cambiamos al modo 12h estando en AM 
----1.4)Esperamos a una hora cualquiera y cambiamos a modo 24h en PM
----1.5)Esperamos a otra hora cualquiera y cambiamos a modo 12h en PM
------(extra) -> deberiamos probar el cambio de modo en todas las horas donde sea interesante (1>13, 8>20, ...)
--
--2)Probar la modificacion del campo horas y del campo minutos, mediante la incrementacion(uno a uno y puls_largo)
--  (al hacer cada una de las operaciones entrar al modo programacion y al acabar la operacion salir, para comprobar
--   que se entra y se sale bien del modo)
----2.1)Campo minutos
------2.1.1)Probamos a incrementar los minutos uno a uno con pulsaciones cortas (comprobar si cuenta bien y si 
------      al llegar al final incrementa las horas y pone a cero los minutos)
------2.1.2)Probamos a incrementar los minutos mediante pulsaciones largas, dando una vuelta completa (comprobar si cuenta bien y si 
------      al llegar al final incrementa las horas y pone a cero los minutos)
----2.2)Campo horas (modo 12H)
------2.2.1)Probamos a incermentar las horas de una en una mediante pulsaciones cortas (comprobar si cuenta bien,
------      si da la vuelta correctamente y si AM_PM cambia correctamente)
------2.2.2)Probamos a incrementar las horas mediante pulsaciones largas, dando una vuelta completa(24H)(comprobar si cuenta bien,
------      si da la vuelta correctamente y si AM_PM cambia correctamente)
--3)Probar la carga de datos en los campos horas y minutos mediante la pulsacion de los valores en el teclado
----3.1)Campo minutos-> todos los valores <= 59 deberian poder cargarse correctamente en cualquier modo.
----3.2)Campo horas
------3.2.1)Modo 12h -> los valores <= 11 deberian cargarse sin problema.
------3.2.2)Modo 24h -> los valores <=23 deberian cargarse sin problema (comprobar que AM_PM cambia correctamente)
-----------------------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

use work.pack_test_reloj.all;

entity test_estimulos_reloj is
port(clk:     	  in std_logic;
     nRst:        in std_logic;
     tic_025s:    out std_logic;
     tic_1s:      out std_logic;
     ena_cmd:     out std_logic;
     cmd_tecla:   out std_logic_vector(3 downto 0);
     pulso_largo: out std_logic;
     modo:        in std_logic;
     segundos:    in std_logic_vector(7 downto 0);
     minutos:     in std_logic_vector(7 downto 0);
     horas:       in std_logic_vector(7 downto 0);
     AM_PM:       in std_logic;
     info:        in std_logic_vector(1 downto 0)
    );
end entity;

architecture test of test_estimulos_reloj is

begin
  -- Tic para el incremento continuo de campo. Escalado. 
  process
  begin
    tic_025s <= '0';
    for i in 1 to 3 loop
       wait until clk'event and clk = '1';
    end loop;

    tic_025s <= '1';
    wait until clk'event and clk = '1';

  end process;
  -- Tic de 1 seg. Escalado.
  process
  begin
    tic_1s <= '0';
    for i in 1 to 15 loop
       wait until clk'event and clk = '1';
    end loop;

    tic_1s <= '1';
    wait until clk'event and clk = '1';

  end process;


  process
  begin
    ena_cmd  <= '0';
    cmd_tecla <= (others => '0');
    pulso_largo <= '0';

    -- Esperamos el final del Reset
    wait until nRst'event and nRst = '1';

    for i in 1 to 9 loop
       wait until clk'event and clk = '1';
    end loop;

    -- Cuenta en formato de 12 horas
    wait until clk'event and clk = '1';

      -------------------------------------TEST 1--------------------------
      ---COMPROBACIÓN DEL NORMAL FUNCIONAMIENTO EN EL MODO 12H Y 24 HORAS---
      report "INICIANDO TEST_1";

      -----MODO 12 HORAS---------
      report "PROBANDO FUNCIONALIDAD EN MODO 12H";

      esperar_hora(horas, minutos, AM_PM, clk, '0', X"11" & X"59"); --Espera a las 11:59 Am      
      report "Esperando a las 11:59 AM (HORA CRiTICA)";

      esperar_hora(horas, minutos, AM_PM, clk, '1', X"11" & X"59"); --Espera a las 11:59 pm
      report "Esperando a las 11:59 PM (HORA CRiTICA)";

      esperar_hora(horas, minutos, AM_PM, clk, '0', X"00" & X"02"); --Se espera un tiempo para asegurar que se raealiza el paso de PM a AM
      report "Esperando a las 00:02 AM (CAMBIO DE PERIODO PM -> AM)";

      cambiar_modo_12_24(ena_cmd, cmd_tecla, clk); --Pulsación de la tecla para cambiar de modo 12H a 24H
      report "Cambio de modo horario realizado (12H -> 24H)";


      -----MODO 24 HORAS---------
      report "PROBANDO FUNCIONALIDAD EN MODO 24H";

      esperar_hora(horas, minutos, AM_PM, clk, '0', X"11" & X"59"); --Espera a las 11:59      
      report "Esperando a las 11:59 (HORA CRiTICA)";

      esperar_hora(horas, minutos, AM_PM, clk, '1', X"23" & X"59"); --Espera a las 23:59 
      report "Esperando a las 23:59 PM (HORA CRiTICA)";

      esperar_hora(horas, minutos, AM_PM, clk, '0', X"00" & X"02"); --Se espera un tiempo para asegurar que se raealiza el paso de 23:59 a 00:02
      report "Esperando a las 00:02 (CAMBIO DE DiA)";

      cambiar_modo_12_24(ena_cmd, cmd_tecla, clk); --Pulsación de la tecla para cambiar de modo 24H a 12H
      report "Cambio de modo horario realizado (24H -> 12H)";
      
      report "TEST_1 FINALIZADO CORRECTAMENTE";

                  -----FIN TEST 1-----


     -------------------------------------TEST 2---------------------------------------
    -----------COMPROBACIÓN MODO PROGRAMACIÓN, INTRODUCCIÓN DE VALORES ----------------

    report "INICIANDO TEST_2";

    entrar_modo_prog(pulso_largo, cmd_tecla, clk); ---entra en en modo programación
    report "Entrando en modo programacion";

    programar_hora_inc_corto(ena_cmd, cmd_tecla, horas, minutos, AM_PM, clk,'0', X"11" & X"50"); --Programación de la hora  11:50AM mediante pulsaicones cortas
    report "Programando 11:50 AM mediante pulsaciones cortas";

    fin_prog(ena_cmd, cmd_tecla, clk); --- Sale del modo programación
    report "Saliendo del modo programacion";
    -----
    report "PRRIMERA PROGRAMACIoN REALIZADA, A CONTINUACIoN SE REALIZARa LA SEGUNDA PROGRAMACIoN";

    entrar_modo_prog(pulso_largo, cmd_tecla, clk); ---entra en en modo programación
    report "Entrando en modo programacion";

    programar_hora_inc_corto(ena_cmd, cmd_tecla, horas, minutos, AM_PM, clk,'1', X"10" & X"30"); --Programación de la hora  10:30PM mediante pulsaicones cortas
    report "Programando 10:30 PM mediante pulsaciones cortas";

    fin_prog(ena_cmd, cmd_tecla, clk); --- Sale del modo programación
    report "Saliendo del modo programacion";

    report "SEGUNDA PROGRAMACIoN REALIZADA, A CONTINUACIoN SE REALIZARa LA TERCERA PROGRAMACIoN";
    
    -----

    entrar_modo_prog(pulso_largo, cmd_tecla, clk); ---entra en en modo programación
    report "Entrando en modo programacion";

    programar_hora_directa(ena_cmd,cmd_tecla, clk, X"11" & X"58");
    report "Se ha introducido una hora en VALOR CORRECTO directamente, se han establecido las 11:58";

    programar_hora_directa(ena_cmd,cmd_tecla, clk, X"50" & X"70");
    report "Se ha introducido una hora en VALOR INCORRECTO directamente, se han establecido las 50:70";

    fin_prog(ena_cmd, cmd_tecla, clk); --- Sale del modo programación
    report "Saliendo del modo programacion";

    ---

    report "TERCERA PROGRAMACIoN REALIZADA";
    

    report "TEST_2 FINALIZADO CORRECTAMENTE";

     -----FIN TEST 2-----
   
              
    assert false
    report "TEST_BENCH FINALIZADO CORRECTAMENTE"
    severity failure;
  end process;

end test;
